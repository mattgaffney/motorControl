#define encoderA  52
#define encoderB  53

#define mRadPerRotation (3141590*2)
#define TicksPerRotation 564

int rotV;
  
void setup() {
Serial.begin(9600);
pinMode(encoderA, INPUT);
pinMode(encoderB, INPUT);
}

void loop() {
  rotV = measureSpeed();
  Serial.println(rotV);
}

/**
 * Precondition: the number of ticks in the time must not exceed 683 to prevent overflow
 * 
 * Postcondition: returns the rotational velocty of the motor in milli radians per second
 */
int measureSpeed(){
  unsigned long testTime = 200;
  unsigned long ticks = countTicks(testTime);
    Serial.print(ticks);
    Serial.print("     ");
  return ((unsigned long)ticks * mRadPerRotation)/(testTime*TicksPerRotation);
  
  }

  /**
   * Postcondtion: returns the number of encoder ticks in the time period measurementTime in milliseconds
   */

int countTicks(int measurementTime){
    int endTime  = measurementTime + millis();
    int newA, newB, a, b, ticks;

    ticks = 0;
    a = digitalRead(encoderA);
    b = digitalRead(encoderB);
    while(endTime>millis()){
       newA = digitalRead(encoderA);
       newB = digitalRead(encoderB);
        if((a != newA )|| (b != newB)){
          a = newA;
          b = newB;
          ticks ++;
        }
    }
    return ticks;
  }
