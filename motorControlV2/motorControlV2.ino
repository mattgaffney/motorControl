#define motor1 0
#define motor2 16
#define motor3 3
#define motor4 4

#define encoder1 5
#define encoder2 6
#define encoder3 7
#define encoder4 8

#define motorAccelerationConst 5


struct motor{
  byte setDir; // direction desirred 
  byte measuredDir; // direction measured by encoder
  
  int measuredSpeed;
  int desiredSpeed;
  int encoderPin;  

  int PWMSpeed;

  int motorPins[2]; // Connected to half bridges to control motor speed and direction
  };

struct motor motors[4];
    
void setup() {
Serial.begin(9600);
}

void loop() {
  int PWMValue;
  readEncoders();
  for(int i = 0; i< ((sizeof(motors)/sizeof *(motors))); i++){
    motor m = motors[i];
    if(m.setDir != m.measuredDir){            // if going wrong way decellerate
      m.PWMSpeed -= motorAccelerationConst;
        if(m.PWMSpeed<0){
          m.PWMSpeed = 0;
        }
        analogWrite(m.motorPins[m.measuredDir], m.PWMSpeed);
        // set the other motor pin accordingly
      }
    else if (m.desiredSpeed > m.measuredSpeed){//if going to slow speed up
        m.PWMSpeed += motorAccelerationConst;
        if( m.PWMSpeed>255){
           m.PWMSpeed = 255;
        }
        analogWrite(m.motorPins[m.measuredDir], m.PWMSpeed);
        // set the other motor pin accordingly
      }
    else if(m.desiredSpeed  < m.measuredSpeed){ //if going to fast slow down
        m.PWMSpeed -= motorAccelerationConst;
        if(m.PWMSpeed<0){
          m.PWMSpeed = 0;
        }
        analogWrite(m.motorPins[m.measuredDir], m.PWMSpeed);
        // set the other motor pin accordingly
      }
    }

}

void setMotorSpeed(byte motorID, byte newSpeed, byte newDir){
    int motorNo = motorID&3;
    motor m = motors[motorNo];
    m.setDir = newDir;
    m.desiredSpeed = newSpeed;
   }
  
void readEncoders(){

for(int i = 0; i< ((sizeof(motors)/sizeof *(motors))); i++){
  motors[i].measuredSpeed = analogRead(motors[i].encoderPin);
  // measuredDir needs set
  if(motors[i].measuredSpeed ==0){
      motors[i].measuredDir = motors[i].setDir;
    }
  }
  //publish speeds?
  return;
  }
