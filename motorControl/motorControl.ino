#define motor1 0
#define motor2 16
#define motor3 3
#define motor4 4

#define encoder1 5
#define encoder2 6
#define encoder3 7
#define encoder4 8

#define motorAcceleration 5


struct motor{
  byte setDir;
  int measuredSpeed;
  int desiredSpeed;
  int encoderPin;  
}


//pwm stuff
  int motorPins[] = {motor1, motor2, motor3, motor4};
  int motorPinFrequency[] = {0,0,0,0};


//motor properties
  int encoderPins[] = {encoder1, encoder2, encoder3, encoder4};
  int desiredSpeed[4];
  int measuredSpeed[4];
  
  
void setup() {
Serial.begin(9600);
}

void loop() {
  int PWMValue;
  readEncoders();
  for(int i = 0; i< ((sizeof(motorPins)/sizeof *(motorPins))); i++){
    if (desiredSpeed[i] > measuredSpeed[i]){
        motorPinFrequency[i]+= motorAcceleration;
        if(motorPinFrequency[i]>255){
          motorPinFrequency[i] = 255;
        }
        analogWrite(motorPins[i], motorPinFrequency[i]);
        
      }
    else if(desiredSpeed[i] < measuredSpeed[i]){
        motorPinFrequency[i]-= motorAcceleration;
        if(motorPinFrequency[i]<0){
          motorPinFrequency[i] = 0;
        }
        analogWrite(motorPins[i], motorPinFrequency[i]);
        
      }
    }

}

void setMotorSpeed(byte motorID, byte newSpeed, byte dir){
    int motorNo = motorID&3;
    int motor = motorPins[motorNo];
    desiredSpeed = newSpeed;
    
  
  }
  
void readEncoders(){

  measuredSpeed[0] = analogRead(encoder1);
  measuredSpeed[1] = analogRead(encoder2);
  measuredSpeed[2] = analogRead(encoder3);
  measuredSpeed[3] = analogRead(encoder4);
  //publish speeds?
  return;
  }
