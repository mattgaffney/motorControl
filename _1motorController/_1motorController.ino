#define motorAccelerationConst 5

#define mRadPerRotation (3141590*2)
#define TicksPerRotation 564


struct motor{
  byte setDir; // direction desirred 
  byte measuredDir; // direction measured by encoder
  
  int measuredSpeed;
  int desiredSpeed;
  int encoderPin[2];  

  int PWMSpeed;

  int motorPin; // Connected to half bridges to control motor speed and direction
  };

struct motor motors[1];/////////////////////////////////////////////////////
motor m = motors[0];



void setup() {
Serial.begin(9600);

m.motorPin = 8;
m.encoderPin[0] = 52;
m.encoderPin[1] = 53;
m.desiredSpeed = 2000;
}

void loop() {
  int PWMValue;
  readEncoders();
  for(int i = 0; i< ((sizeof(motors)/sizeof *(motors))); i++){
 
    if(m.setDir != m.measuredDir){            // if going wrong way decellerate
      m.PWMSpeed -= motorAccelerationConst;
        if(m.PWMSpeed<0){
          m.PWMSpeed = 0;
        }
      }

      
    else if (m.desiredSpeed > m.measuredSpeed){//if going to slow speed up
        m.PWMSpeed += motorAccelerationConst;
        if( m.PWMSpeed>255){
           m.PWMSpeed = 255;
        }
       
      }

      
    else if(m.desiredSpeed  < m.measuredSpeed){ //if going to fast slow down
        m.PWMSpeed -= motorAccelerationConst;
        if(m.PWMSpeed<0){
          m.PWMSpeed = 0;
        }
      }
      Serial.print(m.desiredSpeed);
      Serial.print("      ");
      Serial.print(m.measuredSpeed);
      Serial.print("      ");
      Serial.println(m.PWMSpeed);
      analogWrite(m.motorPin, m.PWMSpeed);
        // set the other motor pin accordingly
    }

}

void setMotorSpeed(int newSpeed){
    m.desiredSpeed = newSpeed;
   }
  /**
   * Postcondtion: returns the number of encoder ticks in the time period measurementTime in milliseconds
   */

int countTicks(int measurementTime){
    int endTime  = measurementTime + millis();
    int newA, newB, a, b, ticks;

    ticks = 0;
    a = digitalRead(m.encoderPin[0]);
    b = digitalRead(m.encoderPin[1]);
    while(endTime>millis()){
       newA = digitalRead(m.encoderPin[0]);
       newB = digitalRead(m.encoderPin[1]);
        if((a != newA )|| (b != newB)){
          a = newA;
          b = newB;
          ticks ++;
        }
    }
    return ticks;
  }
   
    /**
 * Precondition: the number of ticks in the time must not exceed 683 to prevent overflow
 * 
 * Postcondition: returns the rotational velocty of the motor in milli radians per second
 */
int measureSpeed(){
  unsigned long testTime = 200;
  unsigned long ticks = countTicks(testTime);
  unsigned long s = ((unsigned long)ticks * mRadPerRotation)/(testTime*TicksPerRotation);
/*    Serial.print(ticks);
      Serial.print("      ");
   
      Serial.println(s);*/
  
  return s;
  
  }

void readEncoders(){

 m.measuredSpeed = measureSpeed();
 m.measuredDir = m.setDir;
 // if(motors[i].measuredSpeed ==0){
 //     motors[i].measuredDir = motors[i].setDir;
 //   }
  //}
  //publish speeds?
  return;
  }



